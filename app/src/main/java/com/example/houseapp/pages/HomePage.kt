package com.example.houseapp.pages

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.TextView
import android.widget.Toast
import com.example.houseapp.R
import com.example.houseapp.storage.Result

class HomePage : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.home_page)
        setupButtonsListeners(R.id.homePage_start_button, R.string.homePage_start_button_action)
        setupButtonsListeners(
            R.id.homePage_addContact_button,
            R.string.homePage_add_contact_button_action
        )

    }
    private fun setupButtonsListeners(buttonId: Int, titleId: Int) {
        findViewById<TextView>(buttonId).setOnClickListener() {
            Result.setActivatedButtonID(buttonId)
            Result.setTitle(getText(titleId).toString())
            var intention = Intent(applicationContext, ConfirmationPage::class.java)
            startActivity(intention)
        }
    }

    override fun onResume() {
        super.onResume()
        Log.d("TAG", "On Resume")

        if (Result.getIsConfirmed()) {
            Result.setIsConfirmed(false)
            Result.setTitle("")
            var activatedButtonID = Result.getActivatedButtonID()
            if (activatedButtonID == R.id.homePage_start_button) {
                var intention = Intent(applicationContext, FunctionPage::class.java)
                startActivity(intention)
            }
            if (activatedButtonID == R.id.homePage_addContact_button) {
                var intention = Intent(applicationContext, AddContactPage::class.java)
                startActivity(intention)
            }
            finish()
        }
    }

    override fun onBackPressed() {
        Toast.makeText(applicationContext, getText(R.string.on_back_pressed), Toast.LENGTH_SHORT)
            .show()
    }
}