package com.example.houseapp.pages

import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import com.example.houseapp.R
import com.example.houseapp.storage.Result

class ConfirmationPage : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.confirmation_page)
        checkHeader()
    }

    private fun checkHeader() {
        if (!Result.getTitle().equals("")) {
            findViewById<TextView>(R.id.confirmation_title).text = Result.getTitle()
        }
    }

    override fun onResume() {
        super.onResume()
    }

    fun confirm(view: View) {
        val status = true
        backToPreviousPage(status)
    }

    fun reject(view: View) {
        val status = false
        backToPreviousPage(status)
    }

    fun backToPreviousPage(status: Boolean) {
        Result.setIsConfirmed(status)
        onBackPressed()
    }
}