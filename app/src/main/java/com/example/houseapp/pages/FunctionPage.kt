package com.example.houseapp.pages

import android.content.Intent
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import com.example.houseapp.Activity.ActivityWithMenu
import com.example.houseapp.R
import com.example.houseapp.storage.SupportPhone

class FunctionPage : ActivityWithMenu() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.function_page)
        Toast.makeText(applicationContext, "FUNCTION page", Toast.LENGTH_SHORT)
            .show()
    }

    override fun onBackPressed() {
        Toast.makeText(applicationContext, getText(R.string.on_back_pressed), Toast.LENGTH_SHORT)
            .show()
    }

}