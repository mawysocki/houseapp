package com.example.houseapp.pages

import android.content.ContentValues
import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.example.houseapp.DataBase.BasicCommand
import com.example.houseapp.DataBase.DataBaseHelper
import com.example.houseapp.DataBase.TableInfo
import com.example.houseapp.R
import com.example.houseapp.storage.Result
import com.example.houseapp.storage.SupportPhone

class AddContactPage : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.addcontact_page)
        Toast.makeText(applicationContext, "ADD contact page", Toast.LENGTH_SHORT)
            .show()

        findViewById<EditText>(R.id.addcontact_editTextPhone).addTextChangedListener(object :
            TextWatcher {
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                var editPhone = findViewById<EditText>(R.id.addcontact_editTextPhone)
                findViewById<Button>(R.id.addcontact_save_button).isEnabled = editPhone.text.isNotEmpty()

            }

            override fun afterTextChanged(p0: Editable?) {
            }
        })
    }

    override fun onResume() {
        super.onResume()
        Log.d("TAG", "Add contact - On Resume")
        if (Result.getIsConfirmed()) {
            Result.setIsConfirmed(false)
            Result.setTitle("")
            var activatedButtonID = Result.getActivatedButtonID()
            if (activatedButtonID == R.id.addcontact_save_button) {
                Result.setActivatedButtonID(-1);
                addToDB(findViewById<EditText>(R.id.addcontact_editTextPhone).text.toString())
                var intention = Intent(applicationContext, FunctionPage::class.java)
                startActivity(intention)
            }
            finish()
        }
    }

    private fun addToDB(number: String) {
        val dbHelper = DataBaseHelper(applicationContext)
        val db = dbHelper.writableDatabase
        var value = ContentValues()
        var phoneNumber = number
        value.put("number", phoneNumber)
        db.insertOrThrow(TableInfo.TABLE_NAME, null, value)
        Toast.makeText(applicationContext, "Dodałeś numer: $phoneNumber", Toast.LENGTH_SHORT)
            .show()
        var cursor = db.rawQuery(BasicCommand.SQL_COUNT_PHONES, null)
        cursor.moveToFirst()
        val count: Int = cursor.getInt(0)

        cursor = db.query(TableInfo.TABLE_NAME, arrayOf(TableInfo.TABLE_COLUMN_TITLE), null, null, null, null, null)
        cursor.moveToLast()
        var number = cursor.getString(0)

        cursor.close()
        Log.d("TAG", "DB records: $count")
        Log.d("TAG", "DB last number: $number")
    }
    fun saveContact(view: View) {
        var phoneNumber = findViewById<EditText>(R.id.addcontact_editTextPhone).text

        if (SupportPhone.getPhoneNumber().equals(phoneNumber.toString())) {
            Toast.makeText(applicationContext, "Ten numer już istnieje", Toast.LENGTH_SHORT)
                .show()
        }
        else {
            SupportPhone.setPhoneNumber(phoneNumber.toString())
            val confirmTitle = "Dodajesz numer: $phoneNumber"
            Toast.makeText(applicationContext, "Dodałeś numer: $phoneNumber", Toast.LENGTH_SHORT)
                .show()

            Result.setActivatedButtonID(R.id.addcontact_save_button)
            Result.setTitle(confirmTitle)
            var intention = Intent(applicationContext, ConfirmationPage::class.java)
            startActivity(intention)
        }
    }
}