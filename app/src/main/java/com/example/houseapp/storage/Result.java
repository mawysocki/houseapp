package com.example.houseapp.storage;

public class Result {
    private static int activatedButtonID;
    private static boolean isConfirmed = false;
    private static String title = "";

    public static int getActivatedButtonID() {
        return activatedButtonID;
    }

    public static boolean getIsConfirmed() {
        return isConfirmed;
    }

    public static String getTitle() {
        return title;
    }

    public static void setActivatedButtonID(int value) {
        activatedButtonID = value;
    }

    public static void setIsConfirmed(boolean value) {
        isConfirmed = value;
    }

    public static void setTitle(String value) {
        title = value;
    }
}
