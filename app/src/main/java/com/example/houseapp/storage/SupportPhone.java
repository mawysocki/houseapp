package com.example.houseapp.storage;

public class SupportPhone {

    private static String phoneNumber = "";

    public static String getPhoneNumber() {
        return phoneNumber;
    }

    public static void setPhoneNumber(String phoneNumber) {
        SupportPhone.phoneNumber = phoneNumber;
    }

    public static boolean isCallExist() {
        return !SupportPhone.getPhoneNumber().equals("");
    }
}
