package com.example.houseapp.DataBase

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import android.provider.BaseColumns

object TableInfo: BaseColumns {
    const val TABLE_NAME = "phone"
    const val TABLE_COLUMN_TITLE = "number"
}

object BasicCommand {
    const val SQL_CREATE_TABLE =
        "CREATE TABLE ${TableInfo.TABLE_NAME} (" +
                "${BaseColumns._ID} INTEGER PRIMARY KEY," +
                "${TableInfo.TABLE_COLUMN_TITLE} TEXT NOT NULL)"
    val SQL_COUNT_PHONES = "SELECT COUNT(${TableInfo.TABLE_COLUMN_TITLE}) FROM ${TableInfo.TABLE_NAME}"
    val SQL_ALL_PHONE_NUMBERS = "SELECT ${TableInfo.TABLE_COLUMN_TITLE} FROM ${TableInfo.TABLE_NAME}"
    val SQL_DELETE_TABLE = "DROP TABLE IF EXISTS ${TableInfo.TABLE_NAME}"

}

class DataBaseHelper(context: Context): SQLiteOpenHelper(context, TableInfo.TABLE_NAME, null, 1) {
    override fun onCreate(db: SQLiteDatabase?) {
        db?.execSQL(BasicCommand.SQL_CREATE_TABLE)

    }

    override fun onUpgrade(db: SQLiteDatabase?, p1: Int, p2: Int) {
    }

}