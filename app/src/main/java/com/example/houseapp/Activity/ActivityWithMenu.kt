package com.example.houseapp.Activity

import android.content.Intent
import android.database.sqlite.SQLiteDatabase
import android.net.Uri
import android.view.Menu
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import com.example.houseapp.DataBase.BasicCommand
import com.example.houseapp.DataBase.DataBaseHelper
import com.example.houseapp.DataBase.TableInfo
import com.example.houseapp.R
import com.example.houseapp.pages.AddContactPage
import com.example.houseapp.storage.SupportPhone

open class ActivityWithMenu : AppCompatActivity() {
    var phoneRecordsCount = 0
    lateinit var db: SQLiteDatabase

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.call2action, menu)
        val dbHelper = DataBaseHelper(applicationContext)
        db = dbHelper.readableDatabase

        var cursor = db.query(TableInfo.TABLE_NAME, null, null, null, null, null, null)
        phoneRecordsCount = cursor.count


        if (phoneRecordsCount > 0) {
            menu?.getItem(0)?.title = getText(R.string.call_button)
        } else {
            menu?.getItem(0)?.title = getText(R.string.homePage_add_contact_button)
        }
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item?.itemId == R.id.call2_button) {
            if (phoneRecordsCount > 0) {
                var cursor = db.query(TableInfo.TABLE_NAME, arrayOf(TableInfo.TABLE_COLUMN_TITLE), null, null, null, null, null)
                cursor.moveToLast()
                var phoneNumber = cursor.getString(0)
                call(phoneNumber, Intent.ACTION_DIAL)
            } else {
                var intention = Intent(applicationContext, AddContactPage::class.java)
                startActivity(intention)
            }
        }
        return super.onOptionsItemSelected(item)
    }

    private fun call(phone: String, action: String) {
        val intent = Intent()
        intent.data = Uri.parse("tel:$phone")
        intent.action = action
        startActivity(intent, null)
    }

}